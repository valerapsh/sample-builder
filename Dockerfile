FROM nginx
COPY --chown=nginx:nginx html /html
COPY nginx.conf /etc/nginx/nginx.conf